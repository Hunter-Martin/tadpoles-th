import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'bulma/css/bulma.css';

class NotFound extends Component {
  render() {
    return (
      <Link to="/">
        <button className="is-link">Back to home</button>
      </Link>
    );
  }
}

export default NotFound;
