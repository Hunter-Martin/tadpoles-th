import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'bulma/css/bulma.css';
import Home from './components/layouts/Home';
import Configs from './components/dj/Configs';
import NotFound from './NotFound';
import RoomCode from './components/partyMember/RoomCode';
import Dashboard from './components/layouts/Dashboard';

export const authEndpoint = 'https://accounts.spotify.com/authorize';
export const clientID = 'd55852cd79454a2699530591b0c6fc12';
export const redirectURI = process.env.NODE_ENV === 'development' ?
  'http://localhost:3000/' : 'http://albump.xyz/'; // Issue here?
export const scopes = [ // What permissions the user needs
  "streaming",
  "user-top-read",
  "user-read-email",
  "user-read-private",
  "user-read-currently-playing",
  "user-read-playback-state",
  "user-modify-playback-state",
];


// eslint-disable-next-line react/prefer-stateless-function
class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path='/auth-create' component={() => {
            window.location.href = `${authEndpoint}?client_id=${clientID}&redirect_uri=${redirectURI}create&scope=${scopes.join(
              "%20"
            )}&response_type=token&show_dialog=true`;
            return null;
          }}/>
          <Route path='/auth-join' component={() => {
            window.location.href = `${authEndpoint}?client_id=${clientID}&redirect_uri=${redirectURI}join&scope=${scopes.join(
              "%20"
            )}&response_type=token&show_dialog=true`;
            return null;
          }}/>
          {/* <Route path="/login" exact component={Configs} /> */}
          <Route path="/create" exact component={Configs} />
          <Route path="/create/:id" component={Dashboard} />
          <Route path="/login" />
          <Route path="/join" exact component={RoomCode} />
          <Route path="/join/:id" component={Dashboard} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
