import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import hash from '../../hash';

const SpotifyWebApi = require('spotify-web-api-js');

// Most of the Authentication flow code was taken from
// https://github.com/JoeKarlsson/react-spotify-player/
// Bits of this code can also be found in:
// hash.js, RoomCode.js and App.js (auth routes)

class Configs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noAuth: false, // used to determine if user has authenticated or not (i.e. entered url directly/app reloads)
      token: null,
      userId: 'N/A',
      partyName: '',
      party: null,
      maxQueueSize: 100,
      maxPartySize: 100,
    };

    this.spotify = new SpotifyWebApi();

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // Get token from Spotify redirect via `window`
    let _token = hash.access_token;

    if (_token) {
      // If there is a token, set it
      this.setState({
        token: _token
      }, () => {
        this.getUserAcctInfo();
      });
    }
    else {
      this.setState({
        noAuth: true,
      });
    }
  }

  // TODO: Make this function more generic if we ever integrate multiple
  //  platforms to listen on. Maybe make it unrealated to listening platform?
  getUserAcctInfo() {
    this.spotify.setAccessToken(this.state.token);
    this.spotify.getMe().then(res => {
      if (res) {
        this.setState({
          userId: res.id,
        });
      }
    });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    }, () => {
      // console.log(this.state);
    });
  };

  handleSubmit() {
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

    fetch(process.env.NODE_ENV === 'development' ? 'http://localhost:80/create' : 'http://albump.xyz/create', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify ({
        djId: this.state.userId,
        partyName: this.state.partyName !== '' ? this.state.partyName : 'Hollapalooza',
        partySize: this.state.maxPartySize,
        queueSize: this.state.maxQueueSize,
      }),
    }).then(res => res.json())
      .then(response => {
        this.setState({
          party: response.party,
        });
      });
  };

  render() {
    if (this.state.party !== null) {
      // Redirect programmatically after response from backend
      return <Redirect to={{
        pathname: `/create/${this.state.party.partyCode}`,
        authToken: this.state.token,
        partyCode: this.state.party.partyCode,
        party: this.state.party,
        userId: this.state.userId,
      }}/>;
    }
    if (this.state.noAuth) {
      return <Redirect to={'/'}/>
    }
    return (
      <div className="container widen">
        <h1 className="title is-1 has-text-light">Create a New Party</h1>
        <div className="columns center">
          <div className="column squish">
            <div className="field">
              <label className="label has-text-light">Party Name</label>
              <div className="control">
                <input onChange={this.handleChange} value={this.state.partyName} name="partyName" className="input" type="text" placeholder="Hollapalooza" />
              </div>
            </div>

            <div className="field">
              <label className="label has-text-light">Max Queue Size</label>
              <div className="control">
                <input onChange={this.handleChange} value={this.state.maxQueueSize} name="maxQueueSize" className="input" type="number" placeholder="Enter maximum song queue size..." />
              </div>
            </div>

            <div className="field">
              <label className="label has-text-light">Party Size</label>
              <div className="control">
                <input onChange={this.handleChange} value={this.state.maxPartySize} name="maxPartySize" className="input" type="number" placeholder="Enter maximum party size..." />
              </div>
            </div>

            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link" onClick={this.handleSubmit}>Submit</button>
              </div>
              <div className="control center">
                <Link to="/"><button className="button is-link is-light">Cancel</button></Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Configs;
