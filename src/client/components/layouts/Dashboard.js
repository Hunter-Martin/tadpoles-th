import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import WebPlayer from '../WebPlayer';
import InfoBar from './InfoBar';
import Queue from '../queue/Queue';
import SearchResults from "../searchResults/SearchResults";
import io from 'socket.io-client'

let classNames = require('classnames');

const SpotifyAPI = require('spotify-web-api-js');

class Dashboard extends Component {
  constructor(props) {
    super(props);

    if (this.props.location.party !== undefined) { // in case user refreshes page or directly enters link w/o auth
      // If someone finds a better way to do this please refactor
      this.state = {
        authToken: this.props.location.authToken,
        party: this.props.location.party,
        partyCode: this.props.location.partyCode,
        partyName: this.props.location.party.partyConfig.partyName, // Getting lengthy here 🥵
        partyMembers: this.props.location.party.members,
        songs: this.props.location.party.queue.songList,
        userId: this.props.location.userId,
        item: {
          album: {
            images: [{ url: '' }]
          },
          name: '',
          artists: [{ name: '' }],
          duration_ms: 0,
        },
        is_playing: 'Paused',
        progress_ms: 0,
        searchSong: '',
        searchResults: [],
        songTitle: 'Do I wanna know???',
        songArtist: 'The Stokes',
        songAlbum: 'Angles',
        albumArt: '',
        songProgress: 80,
        songUID: '',
      };
    }
    else {
      this.state = {
        authToken: null,
        party: null,
        partyCode: null,
        songs: [],
        item: {
          album: {
            images: [{ url: '' }]
          },
          name: '',
          artists: [{ name: '' }],
          duration_ms: 0,
        },
        is_playing: 'Paused',
        progress_ms: 0,
        searchSong: '',
        searchResults: [],
        songTitle: 'Do I wanna know???',
        songArtist: 'The Stokes',
        songAlbum: 'Angles',
        albumArt: '',
        songProgress: 80,
        songUID: '',
        partyName: null,
      };
    }

    if (this.state.authToken !== null) {
      this.spotify = new SpotifyAPI();
      this.spotify.setAccessToken(this.state.authToken);
    }

    // Sockets

    // TODO: verify sockets connect in production
    if (process.env.NODE_ENV === 'development')
      this.socket = io('http://localhost:80/');
    else
      this.socket = io('http://albump.xyz/'); // TODO: Figure our what the real address is

    this.socket.on('connect', () => {
      console.log('socket connected!');
      this.socket.emit('join room', this.state.partyCode);
    });
    this.socket.on('room msg', (msg) => {
      console.log(msg);
    });
    this.socket.on('queue updated', (queue) => {
      this.setState({
        songs: queue,
      });
    });

    // DANGER: I DON'T KNOW IF THIS WORKS
    this.socket.on('next song', (nextSong, queue) => {
      // console.log('party leader has moved to a new song:', nextSong, queue);

      if (this.state.item.name !== nextSong.title) { // Need to update
        let options = {context_uri: nextSong.album.uri, offset: {uri: nextSong.platformSongId}};
        this.spotify.play(options).then((err, data) => {
          if (err) console.log('Dashboard:', err);
          // else console.log('WebPlayer:', data);
        });

        // Update local queue
        this.setState({
          songs: queue,
        })
      }
    });

    this.addSongToQueue = this.addSongToQueue.bind(this);
    this.popSongFromQueue = this.popSongFromQueue.bind(this);
    this.getCurrentlyPlaying = this.getCurrentlyPlaying.bind(this);
    this.handleVote = this.handleVote.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getCurrentlyPlaying();
    this.interval = setInterval(() => this.getCurrentlyPlaying(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getCurrentlyPlaying() {
    if (this.spotify !== undefined ) { // strange edge case when developing
      this.spotify.getMyCurrentPlaybackState()
        .then(res => {
          if (res) {
            this.setState({
              item: res.item,
              is_playing: res.is_playing,
              progress_ms: res.progress_ms,
            }, () => {
              // console.log('Currently playing:', this.state);
            });
          }
        });
    }
    else {
      // Force the page to return to home
      this.setState({
        authToken: null,
      });
    }
  }

  addSongToQueue(song) {
    this.setState({
      searchResults: [],
    });

    this.socket.emit('add to queue', {song: song, partyCode: this.state.partyCode});
  }

  popSongFromQueue() {
    this.socket.emit('pop queue', {partyCode: this.state.partyCode, userId: this.state.userId});
  }

  handleVote(song, value) {
    console.log('vote: ' + value);
    console.log(song);

    this.socket.emit('vote', {song: song, value: value, partyCode: this.state.partyCode});
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = () => {
    // Use Spotify WebAPI to search for songs matching query string
    // console.log('Searching Spotify tracks for:', this.state.searchSong);
    // this.socket.emit('msg', this.state.searchSong);
    this.spotify.searchTracks(this.state.searchSong,
      { type: 'track' })
      .then((data) => {
        // console.log(data.tracks.items);
        this.setState({
          searchResults: data.tracks.items,
          searchSong: '',
        });
      });
  };

  render() {
    if (!this.state.authToken) {
      console.log('No auth token detected');
      return <Redirect to={'/'}/>
    }
    return (
      <div>
        <WebPlayer
          accessToken={this.state.authToken}
          queue={this.state.songs}
          api={this.spotify}
          socket={this.socket}  // May not be needed
          popQueue={this.popSongFromQueue}
        />
        <InfoBar
          partyName={this.state.partyName}
          partyCode={this.state.partyCode}
          partyMemberCount={this.state.partyMembers.length}
        />
          <div className="columns reverse-columns">
            <div className="column is-two-thirds center ">
              <div className="content">
                <div className="search-container">
                  <div className="columns">
                    <div className="column is-one-third">
                      <div className="field has-addons">
                        <p className="control">
                            <input onChange={this.handleChange} value={this.state.searchSong} name="searchSong" className="input" type="text" placeholder="Add a song" />
                        </p>
                        <p className="control">
                          <button className="button" onClick={this.handleSubmit}>
                              Search
                          </button>
                        </p>
                      </div>
                    </div>
                    <div className="column is-two-thirds">
                      <div className="search-scrollable-content">
                          <SearchResults results={this.state.searchResults} addToQueue={this.addSongToQueue}/>
                      </div>
                    </div>
                  </div>
                  {/* Do we still want this here? */}
                  <progress
                    className="progress is-danger"
                    value={(this.state.progress_ms / this.state.item.duration_ms) * 100}
                    max="100"
                  />
                </div>
                <p className='queue-header'>Queue</p>
                <div className="scrollable-content">
                  {this.state.songs.length > 0 &&
                    <Queue songs={this.state.songs} handleVote={this.handleVote}/>
                  }
                  {this.state.songs.length === 0 &&
                    <span>There's nothing here :(</span>
                  }
                    {/*<Queue songs={[1]} />*/}
                </div>
              </div>
            </div>
          <div className="column is-one-third center">
            <div className="content">
              <div className="current-song-card">
                <div className="card-image">
                  <figure className="image is-4by3">
                    <img src={this.state.item.album.images[0].url} alt="AlbumArtwork"/>
                  </figure>
                  <div className="card-content">
                    <div className="content">
                      <h1 className="title is-5 has-text-light">{this.state.item.name}</h1>
                      <h1 className="title is-6 has-text-light">
                        {this.state.item.artists[0].name}
                        {' '}
                          –
                        {' '}
                        {this.state.item.album.name}
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
              <progress
                className="progress is-danger"
                value={this.state.progress_ms ? (this.state.progress_ms / this.state.item.duration_ms) * 100 : 0}
                max="100"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
