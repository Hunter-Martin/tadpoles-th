import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'bulma/css/bulma.css';

class Home extends Component {

  render() {
    return (
      <div>
        <div className="columns widen is-3">
          <div align="center" className="column has-background-primary vertical-center">
            <Link to="/auth-create">
              <section className="hero widen">
                <div className="hero-body">
                  <div className="container">
                    <h1 className="title">
                    Create a Party
                    </h1>
                    <h2 className="subtitle">
                    Run the party - invite your friends.
                    </h2>
                  </div>
                </div>
              </section>
            </Link>
          </div>
          <div align="center" className="column has-background-danger vertical-center">
            <Link to="/auth-join">
              <section className="hero widen">
                <div className="hero-body">
                  <div className="container">
                    <h1 className="title">
                      Join a Party
                    </h1>
                    <h2 className="subtitle">
                      Join the party - queue your music.
                    </h2>
                  </div>
                </div>
              </section>
            </Link>
          </div>
        </div>
        <h1 className="title is-1 has-text-light">Albump!</h1>
        <p>Albump is a collaborative listening platform developed at TAMUHack 2020. We envision a
          listening experience
          that takes everyone's taste into account.</p>
      </div>
    );
  }
}

export default Home;
