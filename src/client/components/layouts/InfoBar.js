import React, { Component } from 'react';
import '../../app.css';

class InfoBar extends Component {
  render() {
    return (
      <nav className="navbar albump-purple" role="navigation">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <h1 className="title is-3 has-text-light">{this.props.partyName}</h1>
          </a>
          <a className="navbar-item">
            <h3 className="title is-6 is-family-code has-text-light">
              Party ID:
              {this.props.partyCode}
            </h3>
          </a>
        </div>
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              <a className="button is-danger">
                <strong>Party Members: {this.props.partyMemberCount}</strong>
              </a>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default InfoBar;
