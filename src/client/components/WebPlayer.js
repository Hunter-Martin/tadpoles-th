import React, { Component } from 'react';
import Script from 'react-load-script';

class WebPlayer extends Component {
  state = {
    playerID: null,
    expectedSong: '',
    changingSongs: false,
  };

  componentDidMount() {
    window.onSpotifyWebPlaybackSDKReady = () => {
      this.handleLoadSuccess();
    };
  }

  handleLoadSuccess() {
    const token = this.props.accessToken;
    const player = new window.Spotify.Player({
      name: 'Albump Web',
      getOAuthToken: (cb) => {
        cb(token);
      }
    });

    // Error handling
    player.addListener('initialization_error', ({ message }) => { console.error(message); });
    player.addListener('authentication_error', ({ message }) => { console.error(message); });
    player.addListener('account_error', ({ message }) => { console.error(message); });
    player.addListener('playback_error', ({ message }) => { console.error(message); });

    // TODO: This code needs a refactor, but until it's functionality is verified to be correct,
    //  we cannot change it without risking breaking something. I will come back and refactor it
    //  once I am confident in it. - Ben

    // Playback status updates
    player.addListener('player_state_changed', (state) => {
      // console.log('WebPlayer: player_state_changed');
      // console.log(state);
      const currentSong = state.track_window.current_track.uri;
      // this.props.handleNewSong(currentSong);
      const progression = (state.position / state.duration) * 100;
      // console.log(progression);

      if (progression > 98 && !this.state.changingSongs) {
        // Need to change songs from queue
        if (this.props.queue.length > 0) {
          // console.log('WebPlayer: playing next from queue: ' + this.props.queue[0].title);
          let nextSong = this.props.queue[0];
          // console.log(nextSong);

          // ********** DON'T DELETE (YET)! MAY STILL BE NEEDED ********** //

          // let currentSpotifyQueue = this.props.queue.map(song => song.uri);

          // this.setState({
          //   changingSongs: true,
          //   expectedSong: nextSong.uri,
          // }, () => {
            let options = {context_uri: nextSong.album.uri, offset: {uri: nextSong.platformSongId}};
            this.props.api.play(options).then((err, data) => {
              if (err) console.log('WebPlayer:', err);
              // else console.log('WebPlayer:', data);
            });

            // Update local queue
            this.props.queue.shift();
            this.props.popQueue();
          // });

        }
      }

      // ********** DON'T DELETE (YET)! MAY STILL BE NEEDED ********** //

      // if (currentSong === this.state.expectedSong) {
      //   this.setState({
      //     expectedSong: '',
      //     changingSongs: false,
      //   });
      // }
    });

    // Ready
    player.addListener('ready', ({ device_id }) => {
      console.log('WebPlayer: ready');

      // Pause playback if user is playing on another device
      this.props.api.pause().then((err, data) => {
        if (err) console.error(err, data);
        else console.log('WebPlayer: paused playback on current device', data);
      });

      // Transfer playback to WebPlayer
      this.props.api.transferMyPlayback([device_id], { play: true })
        .then((err, data) => {
          if (err) console.error(err);
          else console.log('WebPlayer: transferred user playback');
        });

      this.setState({
        playerID: device_id,
      });
      // this.props.socket.emit('player ready', device_id);
    });

    // Not Ready
    player.addListener('not_ready', ({ device_id }) => {
      console.log('WebPlayer: device ID has gone offline', device_id);
    });

    // Connect to the player!
    player.connect()
      .then((success) => {
      if (success) {
        console.log('WebPlayer: successfully connected to Spotify');
      } else {
        console.log('WebPlayer: error connecting to Spotify');
      }
    });
  }

  render() {
    return (
      <div className="container">
        <header>
          <Script
            url="https://sdk.scdn.co/spotify-player.js"
          />
        </header>
      </div>
    );
  }
}

export default WebPlayer;
