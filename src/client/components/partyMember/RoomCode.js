import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import ReactCodeInput from 'react-verification-code-input';
import hash from '../../hash';

const SpotifyWebApi = require('spotify-web-api-js');

// Most of the Authentication flow code was taken from
// https://github.com/JoeKarlsson/react-spotify-player/
// Bits of this code can also be found in:
// hash.js, Config.js and App.js (auth route)

class RoomCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noAuth: false, // used to determine if user has authenticated or not (i.e. entered url directly/app reloads)
      token: null,
      partyCode: null,
      party: null,
      userId: 'N/A',
      tryAgain: false
    };

    this.spotify = new SpotifyWebApi();

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // Set token
    let _token = hash.access_token;

    if (_token) {
      // Set token
      this.setState({
        token: _token
      }, () => {
        this.getUserAcctInfo();
      });
      // this.getCurrentlyPlaying(_token);
    }
    else {
      this.setState({
        noAuth: true,
      });
    }
  }

  getUserAcctInfo() {
    this.spotify.setAccessToken(this.state.token);
    this.spotify.getMe().then(res => {
      if (res) {
        this.setState({
          userId: res.id,
        });
      }
    });
  }

  handleChange = (vals) => {
    this.setState({
      partyCode: vals,
    });
  };

  // TODO: change `joinerId` from 'test_user' to the username of whichever spotify acct is joining
  handleSubmit = (event) => {
    console.log('Connecting to a party with code ', this.state.partyCode.toString());

    fetch(process.env.NODE_ENV === 'development' ? 'http://localhost:80/join' : 'http://albump.xyz/join', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify ({
        joinerId: 'test_user',
        partyCode: this.state.partyCode,
      }),
    }).then(res => res.json())
        .then(response => {
          if(response.party){
            if(response.party === 'error'){
              this.setState({
                partyCode: null,
                tryAgain: true
              })
            }
            else{
              this.setState({
                party: response.party,
              });
            }
          }
        });
  };



  render() {
    let errorMessage;
    if(this.state.tryAgain){
      errorMessage = <h3>That party code didn't work. Please try again.</h3>
    }

    if (this.state.party !== null) {
      // Redirect programmatically after response from backend
      return(
          <Redirect to={{
            pathname: `/join/${this.state.party.partyCode}`,
            authToken: this.state.token,
            partyCode: this.state.party.partyCode,
            party: this.state.party,
            userId: this.state.userId,
          }}/>
      )
    }

    if (this.state.noAuth) {
      return <Redirect to={'/'}/>
    }
    return (
      <div className="container widen center">
        <h1 className="title is-1 has-text-light">Join a Party</h1>
        <div className="columns is-desktop">
          <div className="column">
            <div className="field">
              <label className="label has-text-light">Party Code</label>
              <div className="control center">
                <ReactCodeInput
                  type="text"
                  fields={8}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="field left">{errorMessage}</div>
            <div className="field is-grouped">
              <div className="control">
                <button disabled={!this.state.partyCode} className="button is-link" onClick={this.handleSubmit}>Submit</button>
              </div>
              <div className="control">
                <Link to="/"><button className="button is-link is-light">Cancel</button></Link>
              </div>
            </div>
          </div>
          <div className="column">
            <img src={require('../../graphic1.svg')} />
          </div>
        </div>

      </div>
    );
  }
}

export default RoomCode;
