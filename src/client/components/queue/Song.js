import React, {Component} from 'react';
let classNames = require('classnames');

class Song extends Component {
    constructor(props) {
        super(props);

        this.state ={
            song: this.props.song,
            votes: this.props.song.upvotes + this.props.song.downvotes,
        };

        this.clickVote = this.clickVote.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // console.log('nextProps: ', nextProps);
        this.setState({
            song: nextProps.song,
            votes: nextProps.song.upvotes + nextProps.song.downvotes,
        });
    }

    static defaultProps = {
        songs:  {
            artist: "Artist",
            title: "Title",
            votes: 0,
        }//undefined gets converted to this by default
    };

    clickVote(value) {

    }

    render() {
        // let { song } = this.props;

        // STYLING
        let isLongText = () => {
            return this.state.song.artist.length > 20;
        };

        let isVotesPos = () => {
            return this.state.votes > 0;
        };
        let isVotesNeg = () => {
            return this.state.votes < 0;
        };
        let isVotesZero = () => {
            return this.state.votes === 0;
        };

        let artist_styling = classNames({truncate:isLongText()},'artist');
        let song_styling = classNames({truncate:isLongText()},'song-title');
        let vote_styling = classNames(
            'votes-container',
            'votes',
            {redText:isVotesNeg()},
            {greenText:isVotesPos()},
            {blackText:isVotesZero()}
        );
        //END STYLING

        return (
            <div>
                <div className="card">
                    <div className="card-container">
                        <div className="album-art">
                            <img src={this.state.song.album.images[0].url} />
                        </div>
                        <div className={song_styling}>
                            <p>{this.state.song.title}</p>
                        </div>

                        <div className={artist_styling}>
                            <p>{this.state.song.artist}</p>
                        </div>

                        <div className={vote_styling}>
                            <h3>{this.state.votes ? this.state.votes : 0}</h3>
                        </div>
                    </div>
                    <div className="vote-bar">
                        <button onClick={() => this.props.handleVote(this.state.song, 1)} className="upvote vote-button">Up</button>
                        <button onClick={() => this.props.handleVote(this.state.song, -1)} className="downvote vote-button">Down</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Song;
