import React, {Component} from 'react';
import Song from "./Song";

class Queue extends Component {

    render() {
        let { songs } = this.props;
        return (
            <div>
                {songs &&
                songs.map(song => {
                    return (
                        <div key={song.id}>
                            <Song song={song} handleVote={this.props.handleVote}/>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default Queue;
