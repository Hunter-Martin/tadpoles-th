import React, {Component} from 'react';
let classNames = require('classnames');

class Result extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { title, artist } = this.props;

        let isLongText = (string) => {
            return string.length > 20;
        };

        let artist_styling = classNames({truncate:isLongText(artist)},'search-artist');
        let song_styling = classNames({truncate:isLongText(title)},'search-song-title');
        return (
            <div className="search-card">
                <div className="search-card-container">
                    <div className={song_styling}>
                        <p>{title}</p>
                    </div>

                    <div className={artist_styling}>
                        <p>{artist}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Result;
