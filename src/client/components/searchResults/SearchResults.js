import React, {Component} from 'react';
import Result from "./Result";

class SearchResults extends Component {

    songInfo = (song) => {
        // console.log(song);
        let songObj = {
            type: 'spotify',
            title: song.name,
            artist: song.artists[0].name,
            album: song.album,
            id: song.id,
            uri: song.uri,
            votes: 0,
        };

        return songObj;
    };

    render() {
        const { results } = this.props;
        return (
            <div>
                {results &&
                results.map(result => {
                    return (
                        <div key={result.id} onClick={() => this.props.addToQueue(this.songInfo(result))}>
                            <Result title={result.name}
                                    artist={result.artists[0].name}
                            />
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default SearchResults;
