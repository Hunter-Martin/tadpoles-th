const Song = require('./models/Song.js');

// Reference: https://github.com/JMPerez/c/blob/master/server/api.js
// Reference: https://socket.io/docs/

module.exports = function (socket) {
    // get data
    socket = data.socket;
    roomName = data.partyId;

    // set room
    socket.join(roomName);

    socket.emit('join', { message: 'socket connected' });

    // Add to queue
    socket.on('add to queue', body => {
        // set body variables
        var partyId = body.partyId;
        var platform = body.platform;
        var platformSongId = body.platformSongId;

        // get queue
        var queue = PartyList.findById(partyId).queue;

        // create song
        var song = new Song();
        song.platform = platform;
        song.platformSongId = platformSongId;

        // process push
        queue.vote(singId, voteValue);

        // emit updated queue to clients
        socket.broadcast.to(partyId).emit('queue', { queue: party.queue });
    });

    // Vote on queue
    socket.on('vote', body => {
        // set body variables
        var partyId = body.partyId;
        var songId = body.songId;
        var voteValue = body.voteValue;

        // get queue
        var queue = PartyList.findById(partyId).queue;

        // process vote
        queue.vote(singId, voteValue);

        // emit updated queue to clients
        socket.broadcast.to(partyId).emit('queue', { queue: party.queue });
    });

    // End of Song
    socket.on('end of song', body => {
        // set body variables
        var partyId = body.partyId;

        // get queue
        var queue = PartyList.findById(partyId).queue;

        // get song from pop
        var nextSong = queue.pop();

        // emit updated queue to clients
        socket.broadcast.to(partyId).emit('update', {
            nextSong: nextSong, queue: party.queue
        });
    });
}