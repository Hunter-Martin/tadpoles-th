const GEN_ID = require("../utils.js").GEN_ID;

class PartyConfig {

    constructor(_id, _partyName, _partySize, _queueSize) {
        if (_id === undefined) this.id = GEN_ID();
        else this.id = _id;

        if (_partyName === undefined) this.partyName = null;
        else this.partyName = _partyName;

        if (_partySize === undefined) this.partySize = 0;
        else this.partySize = _partySize;

        if (_queueSize === undefined) this.queueSize = 0;
        else this.queueSize = _queueSize;
    }

    print() {
        console.log(' ---- Party Config ----');
        console.log('id: ', this.id);
        console.log('PartyName: ', this.partyName);
        console.log('PartySize: ',  this.partySize);
        console.log('QueueSize: ', this.queueSize + '\n');
    }
}

module.exports = PartyConfig;
