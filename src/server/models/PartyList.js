class PartyList {
    // static partyList = [];

    static addParty(_party) {
        this.partyList.push(_party);
    }

    static createParty(_partyInfo) {
        // make party from json obj
    }

    static removeParty(_party) {
        const isParty = (element) => element.id === _party.id;
        var removedParty = this.partyList[this.partyList.findIndex(isParty)];
        this.partyList.splice(this.partyList.findIndex(isParty), 1);
        return removedParty;
    }

    static removePartyById(_id) {
        const isParty = (element) => element.id === _id;
        var removedParty = this.partyList[this.partyList.findIndex(isParty)];
        this.partyList.splice(this.partyList.findIndex(isParty), 1);
        return removedParty;
    }

    static findById(_id) {
        const isParty = (element) => element.id === _id;
        return this.partyList[this.partyList.findIndex(isParty)]
    }

    static findByCode(_code) {
        const isParty = (element) => element.partyCode === _code;
        return this.partyList[this.partyList.findIndex(isParty)]
    }

    static printList() {
        console.log(' ------- Party List ------ ');
        for (let i = 0; i < this.partyList.length; i++) {
            console.log('Party: ', this.partyList[i].id);
        }
    }

    // static findSongById(_songId) {
    //     for (let pi = 0; pi < this.partyList.length; pi++) {
    //         var songList = this.partyList[i].queue.songList;
    //         for (let si = 0; si < songList.length; si++) {
    //             if (songList[i].id === _songId)
    //                 return songList[i];
    //         }
    //     }
    //     return null;
    // }

    static getPartyList() {
        return this.partyList;
    }

    static clear() {
        while (this.partyList.length > 0) {
            this.removeParty(this.partyList[0]);
        }
    }

    static print() {
        console.log(' ----------- Party List -----------');
        for (let i = 0; i < this.partyList.length; i++) {
            console.log(this.partyList[i].print());
        }
    }
}

PartyList.partyList = [];

module.exports = PartyList;
