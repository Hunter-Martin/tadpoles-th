const GEN_ID = require("../utils.js").GEN_ID;

class Queue {
    constructor(_id, _songList) {
        if (_id === undefined) this.id = GEN_ID();
        else this.id = _id;

        if (_songList === undefined) this.songList = [];
        else this.songList = _songList;
    }

    push(_song) {
        this.songList.push(_song);
        this.songList = this.mergeSort(this.songList);
    }

    pop() {
        return this.songList.shift();
    }

    vote(_platformSongId, value) {
        let song = this.songList.find(s => s.platformSongId === _platformSongId);

        if (song !== undefined) {
            if (value > 0) song.upvotes += value;
            else song.downvotes += value;

            this.songList = this.mergeSort(this.songList);
        }
    }

    // Reference: https://initjs.org/merge-sort-in-javascript-4614386c1374
    mergeSort(arr) {
        if (arr.length < 2)
            return arr;

        var middle = parseInt(arr.length / 2);
        var left = arr.slice(0, middle);
        var right = arr.slice(middle, arr.length);

        return this.merge(this.mergeSort(left), this.mergeSort(right));
    }

    merge(left, right) {
        var result = [];

        while (left.length && right.length) {
            if (left[0].voteValue() >= right[0].voteValue()) {
                result.push(left.shift());
            } else {
                result.push(right.shift());
            }
        }

        while (left.length)
            result.push(left.shift());

        while (right.length)
            result.push(right.shift());

        return result;
    }

    size() {
        return this.songList.length;
    }

    printSongs() {
        for (let i = 0; i < this.songList.length; i++) {
            this.songList[i].print();
        }
    }

    print() {
        console.log(' ------ Queue ------');
        console.log('Id: ', this.id);
        console.log('Size: ', this.songList.length);
        if (this.songList.length > 0)
            this.printSongs();
        else
            console.log('SongList: empty');
    }
}

module.exports = Queue;
