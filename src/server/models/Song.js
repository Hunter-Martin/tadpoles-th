const GEN_ID = require("../utils.js").GEN_ID;

class Song {
    constructor(_id, _platform, _platformSongId, _title, _artist, _album, _upvotes, _downvotes) {
        if (_id === undefined) this.id = GEN_ID();
        else this.id = _id;

        if (_platform === undefined) this.platform = null;
        else this.platform = _platform;

        if (_platformSongId === undefined) this.platformSongId = null;
        else this.platformSongId = _platformSongId;

        if (_title === undefined) this.title = null;
        else this.title = _title;

        if (_artist === undefined) this.artist = null;
        else this.artist = _artist;

        if (_album === undefined) this.album = null;
        else this.album = _album;

        if (_upvotes === undefined) this.upvotes = 0;
        else this.upvotes = _upvotes;

        if (_downvotes === undefined) this.downvotes = 0;
        else this.downvotes = _downvotes;
    }

    voteValue() {
        return this.upvotes + this.downvotes;
    }

    print() {
        console.log(' --- Song ---');
        console.log('id: ' + this.id);
        console.log('Platform: ' + this.platform);
        console.log('Platform ID: ' + this.platformSongId);
        console.log('Title: ' + this.title);
        console.log('Artist: ' + this.artist);
        console.log('Album: ' + this.album);
        console.log('Upvotes: ' + this.upvotes);
        console.log('Downvotes: ' + this.downvotes + '\n');
    }
}

module.exports = Song;
