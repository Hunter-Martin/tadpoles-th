const GEN_ID = require('../utils.js').GEN_ID;
const GEN_CODE = require('../utils.js').GEN_CODE;
const Queue = require('./Queue.js');
const PartyConfig = require('./PartyConfig.js');
const Song = require('./Song.js');

class Party {
    constructor(_id, _partyCode, _djId, _queue, _partyConfig, _currentSong, _members) {
        if (_id === undefined) this.id = GEN_ID();
        else this.id = _id;

        if (_partyCode === undefined) this.partyCode = GEN_CODE();
        else this.partyCode = _partyCode;

        if (_djId === undefined) this.djId = null;
        else this.djId = _djId;

        if (_queue === undefined) this.queue = new Queue();
        else this.queue = _queue;

        if (_partyConfig === undefined) this.partyConfig = new PartyConfig();
        else this.partyConfig = _partyConfig;

        if (_currentSong === undefined) this.currentSong = new Song();
        else this.currentSong = _currentSong;

        // list of Platform User IDs
        if (_members === undefined) this.members = new Array();
        else this.members = _members;
    }

    addMember(_member) {
        this.members.push(_member);
    }

    removeMember(_member) {
        const isMember = (element) => element === _member;
        var removedMember = this.members[this.members.findIndex(isMember)];
        this.members.splice(this.members.findIndex(isMember), 1);
        return removedMember;
    }

    printMembers() {
        console.log(' -- Members --');
        for (let i=0; i<this.members.length; i++){
            console.log('Member: ', this.members[i]);
        }
    }

    print() {
        console.log(' --------- Party ---------');
        console.log('Id: ', this.id);
        console.log('PartyCode: ', this.partyCode);
        console.log('DjId: ', this.djId);
        console.log(this.queue.print());
        console.log(this.partyConfig.print());
        console.log(this.currentSong.print());
        if (this.members.length > 0)
            this.printMembers();
        else
            console.log('Members: empty');
    }
}

module.exports = Party;
