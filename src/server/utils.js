
// Reference: https://gist.github.com/gordonbrander/2230317
var GEN_ID = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 9);
};

var GEN_CODE = function () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return Math.random().toString(36).substr(2, 8).toUpperCase();
};

module.exports = { GEN_ID: GEN_ID, GEN_CODE: GEN_CODE };
