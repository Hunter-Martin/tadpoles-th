// const app = require('express')();

const express = require('express');
const history = require('connect-history-api-fallback');
const bodyParser = require('body-parser');
const test = require('./test.js');
const Party = require('./models/Party.js');
const Song = require('./models/Song.js');
const PartyConfig = require('./models/PartyConfig.js');
const PartyList = require('./models/PartyList.js');
const Sockets = require('./sockets.js');

const app = express();
const server = require('http').Server(app);

// -----------------------------------------------------------------------------
// App Setup
// -----------------------------------------------------------------------------

app.use(history());
app.use(express.static('dist'));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

server.listen(process.env.PORT || 80, () => console.log(`Listening on port ${process.env.PORT || 80}!`));

const io = require('socket.io')(server);

// Test Backend
// const test_results = test();  // This is broken now because I changed the song model  - Ben

// -----------------------------------------------------------------------------
// Socket Instantiation
// -----------------------------------------------------------------------------

// const sockets = (io) => {
//   io.on('connection', data => {
//       require('./sockets.js');
//   });
// }

io.on('connection', socket => {
  console.log('connected..');

  socket.on('connect', () => {
    console.log('Connected!');
  });

  socket.on('msg', console.log);

  socket.on('join room', (room) => {
    console.log('request to join room ' + room);
    socket.join(room);
    io.to(room).emit('room msg', 'user has joined room ' + room);
  });

  socket.on('vote', (data) => {
    // console.log(data);
    console.log('vote of ' + data.value + ' on "' + data.song.title + '" for party ' + data.partyCode);

    let party = PartyList.findByCode(data.partyCode);
    party.queue.vote(data.song.platformSongId, data.value);

    // party.queue.print();

    io.to(data.partyCode).emit('queue updated', party.queue.songList);
  });

  socket.on('add to queue', (data) => {
    console.log('add song to queue for party ' + data.partyCode + ':');
    console.log(data.song.title, '-', data.song.artist);

    let party = PartyList.findByCode(data.partyCode);
    party.queue.push(
      new Song(
        undefined, 'spotify', data.song.uri,
        data.song.title, data.song.artist, data.song.album, 0, 0
      )
    );

    // party.queue.print();

    io.to(data.partyCode).emit('queue updated', party.queue.songList);
  });

  socket.on('pop queue', (data) => {
    console.log('pop from queue for party ' + data.partyCode);

    let party = PartyList.findByCode(data.partyCode);

    // TODO: Think about possible bugs that this section might be susceptible to
    //  It is possible that the party leader leaves. What then? Etc.
    if (data.userId === party.djId) {  // Party leader changed songs; keep party in sync
      let nextSong = party.queue.pop();

      io.to(data.partyCode).emit('next song', nextSong, party.queue.songList);
    }

    // party.queue.print();
  });

  // require('./sockets.js');
});

// -----------------------------------------------------------------------------
// API endpoints
// -----------------------------------------------------------------------------

// req details:
//  - djId
//  - partyConfig
app.post('/create', (req, res) => {
  console.log('Create a party');
  // Set req params
  const { djId } = req.body;
  const { partyName } = req.body;
  const { partySize } = req.body;
  const { queueSize } = req.body;

  // Create partyConfig
  const partyConfig = new PartyConfig();
  partyConfig.partyName = partyName;
  partyConfig.partySize = partySize;
  partyConfig.queueSize = queueSize;

  // Create party
  const newParty = new Party();
  newParty.djId = djId;
  newParty.partyConfig = partyConfig;

  // Add creator to party
  newParty.addMember(djId);

  // console.log(newParty);

  // Add party to party db list
  PartyList.addParty(newParty);

  PartyList.print();
  // Send msg
  res.send({
    message: 'Successfully created a party',
    party: newParty,
  });
});


// req details:
//  - joiner ID
//  - partyCode
// TODO: Make this work (not fully "hooked up")
//  Specifically, more error checking etc. Currently, if a user tries to join
//  a non-existant room there is nothing the server does to handle the bad request
app.put('/join', (req, res) => {
  console.log('Join a party');
  // Set req params
  const { joinerId } = req.body;
  const { partyCode } = req.body;

  // var testParty = new Party();
  // testParty.partyCode = partyCode;
  // PartyList.addParty(testParty);

  PartyList.print();

  // Get party
  let party = PartyList.findByCode(partyCode);

  console.log('search party', partyCode, ':', party);
  if (!party){
    console.log("Party code does not exist.");
    res.send({
          message: 'Could not find party.',
          party: 'error'
        }
    )
  }
  else {
    // Add joiner to party
    party.addMember(joinerId);

    PartyList.print();
    // Send msg
    res.send({
      message: 'Successfully joined a party',
      party: party,
    });
  }
});

// Is this used?
app.put('/party', (req, res) => {
  const { partyCode } = req.body;
  const party = PartyList.findByCode(partyCode);
  // party.queue.push(new Song(undefined, 'spotify', 'spotify:track:2Fnl5TWYqdmSwTHvrB7voW', 0, 0));
  // console.log(party);
  res.send(party);
});

