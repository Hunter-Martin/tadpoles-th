const Party = require('./models/Party.js');
const PartyConfig = require('./models/PartyConfig.js');
const Queue = require('./models/Queue.js');
const Song = require('./models/Song.js');
const PartyList = require('./models/PartyList.js');

function test() {
    if (!testModels()) return false;
    console.log('testModels: passed\n');
}

function testModels() {
    if (!testPartyConfig()) return false;
    console.log('testPartyConfig: passed\n');

    if (!testSong()) return false;
    console.log('testSong: passed\n');

    if (!testQueue()) return false;
    console.log('testQueue: passed\n');

    if (!testParty()) return false;
    console.log('testParty: passed\n');

    if (!testPartyList()) return false;
    console.log('testPartyList: passed\n');
}

function testPartyList() {
    var size = PartyList.partyList.length;
    var testParty = new Party();
    var removedParty = new Party();

    // test addParty
    // console.log('\nTesting addParty...');
    for (let i = 0; i < 3; i++) {
        size = PartyList.partyList.length;
        PartyList.addParty(new Party());
        if (PartyList.partyList.length != (size + 1)) {
            console.log('testPartyList (addParty): incorrect size => ', PartyList.partyList.length);
            return false;
        }
    }
    // PartyList.printList();

    // console.log('Testing addParty...');
    // test createParty

    // test removeParty
    // console.log('\nTesting removeParty...');
    PartyList.addParty(testParty);
    size = PartyList.partyList.length;
    // PartyList.printList();
    // console.log('Removing: ', testParty.id);
    removedParty = PartyList.removeParty(testParty);
    // PartyList.printList();

    if (PartyList.partyList.length != (size - 1)) {
        console.log('testPartyList (removeParty): party not removed => ', testParty.id);
        return false;
    }
    if (removedParty.id != testParty.id) {
        console.log('testPartyList (removeParty): incorrect party => ', removedParty.id, ' != ', testParty.id);
        return false;
    }

    // test removePartyById
    // console.log('\nTesting removePartyById...');
    PartyList.addParty(testParty);
    size = PartyList.partyList.length;
    // PartyList.printList();
    // console.log('Removing: ', testParty.id);
    removedParty = PartyList.removePartyById(testParty.id);
    // PartyList.printList();

    if (PartyList.partyList.length != (size - 1)) {
        console.log('testPartyList (removePartyById): party not removed => ', testParty.id);
        return false;
    }
    if (removedParty.id != testParty.id) {
        console.log('testPartyList (removePartyById): incorrect party => ', removedParty.id, ' != ', testParty.id);
        return false;
    }

    // test findById
    // console.log('\nTesting findById...');
    PartyList.addParty(testParty);
    // PartyList.printList();
    foundParty = PartyList.findById(testParty.id);
    if (foundParty.id != testParty.id) {
        console.log('testPartyList (findById): incorrect party => ', foundParty.id, ' != ', testParty.id);
        return false;
    }

    // test findByCode
    // console.log('\nTesting findById...');
    PartyList.addParty(testParty);
    // PartyList.printList();
    foundParty = PartyList.findByCode(testParty.partyCode);
    if (foundParty.id != testParty.id) {
        console.log('testPartyList (findByCode): incorrect party => ', foundParty.code, ' != ', testParty.code);
        return false;
    }

    // TODO: test findSongById

    
    // test clear
    // console.log('\nTesting clear...');
    PartyList.clear();
    if (PartyList.partyList.length > 0) {
        console.log('testPartyList (clear): parties still exit => ', PartyList.partyList.length);
        return false;
    }
    return true;
}

function testParty() {
    // Test empty construction
    var testParty = new Party();

    if (testParty.id == null || testParty.id > 0) {
        console.log('testParty (empty): invalid ID generated => ', testParty.id);
        return false;
    }
    if (testParty.partyCode == undefined) {
        console.log('testParty (empty): incorrect partyCode => ', testParty.partyCode);
        return false;
    }
    if (testParty.djId != null) {
        console.log('testParty (empty): incorrect djId => ', testParty.djId);
        return false;
    }
    if (testParty.queue == null) {
        console.log('testParty (empty): incorrect queue => ', testParty.queue);
        return false;
    }
    if (testParty.partyConfig == null) {
        console.log('testParty (empty): incorrect partyConfig => ', testParty.partyConfig);
        return false;
    }
    if (testParty.currentSong == null) {
        console.log('testParty (empty): incorrect currentSong => ', testParty.currentSong);
        return false;
    }
    if (testParty.members.length > 0) {
        console.log('testParty (empty): incorrect member length => ', testParty.members.length);
        return false;
    }

    // Test full construction
    testQueue = new Queue();
    testPartyConfig = new PartyConfig();
    testSong = new Song();
    testParty = new Party(_id = 'id', _partyCode = 'partyCode', _djId = 'djId', _queue = testQueue, _partyConfig = testPartyConfig, _currentSong = testSong, _members = ['one', 'two', 'three']);

    if (testParty.id != 'id') {
        console.log('testParty (full): invalid ID generated => ', testParty.id);
        return false;
    }
    if (testParty.partyCode != 'partyCode') {
        console.log('testParty (full): incorrect partyCode => ', testParty.partyCode);
        return false;
    }
    if (testParty.djId != 'djId') {
        console.log('testParty (full): incorrect djId => ', testParty.partySize);
        return false;
    }
    if (testParty.queue.id != testQueue.id) {
        console.log('testParty (full): incorrect queue => ', testParty.queue);
        return false;
    }
    if (testParty.partyConfig.id != testPartyConfig.id) {
        console.log('testParty (full): incorrect partyConfig => ', testParty.partyConfig);
        return false;
    }
    if (testParty.currentSong.id != testSong.id) {
        console.log('testParty (full): incorrect currentSong => ', testParty.currentSong);
        return false;
    }
    if (testParty.members.length <= 0) {
        console.log('testParty (full): incorrect member length => ', testParty.members.length);
        return false;
    }
    if (testParty.members[0] != 'one' || testParty.members[1] != 'two' || testParty.members[2] != 'three') {
        console.log('testParty (full): incorrect members => ', testParty.members);
        return false;
    }

    return true;
}

function testPartyConfig() {
    // Test empty construction
    var testPartyConfig = new PartyConfig();

    if (testPartyConfig.id == null || testPartyConfig.id > 0) {
        console.log('testPartyConfig (empty): invalid ID generated => ', testPartyConfig.id);
        return false;
    }
    if (testPartyConfig.partyName != null) {
        console.log('testPartyConfig (empty): incorrect partyName => ', testPartyConfig.partyName);
        return false;
    }
    if (testPartyConfig.partySize != 0) {
        console.log('testPartyConfig (empty): incorrect partySize => ', testPartyConfig.partySize);
        return false;
    }
    if (testPartyConfig.queueSize != 0) {
        console.log('testPartyConfig (empty): incorrect queueSize => ', testPartyConfig.queueSize);
        return false;
    }

    // Test full construction
    testPartyConfig = new PartyConfig('id', 'partyName', 1, 1);

    if (testPartyConfig.id != 'id') {
        console.log('testPartyConfig (full): invalid ID generated => ', testPartyConfig.id);
        return false;
    }
    if (testPartyConfig.partyName != 'partyName') {
        console.log('testPartyConfig (full): incorrect partyName => ', testPartyConfig.partyName);
        return false;
    }
    if (testPartyConfig.partySize != 1) {
        console.log('testPartyConfig (full): incorrect partySize => ', testPartyConfig.partySize);
        return false;
    }
    if (testPartyConfig.queueSize != 1) {
        console.log('testPartyConfig (full): incorrect queueSize => ', testPartyConfig.queueSize);
        return false;
    }

    return true;
}

function testSong() {
    // Test empty construction
    var testSong = new Song();

    if (testSong.id == null || testSong.id > 0) {
        console.log('testSong (empty): invalid ID generated => ', testSong.id);
        return false;
    }
    if (testSong.platform != null) {
        console.log('testSong (empty): incorrect platform => ', testSong.platform);
        return false;
    }
    if (testSong.platformSongId != null) {
        console.log('testSong (empty): incorrect platformSongId => ', testSong.platformSongId);
        return false;
    }
    if (testSong.upvotes != 0) {
        console.log('testSong (empty): incorrect upvotes => ', testSong.upvotes);
        return false;
    }
    if (testSong.downvotes != 0) {
        console.log('testSong (empty): incorrect downvotes => ', testSong.downvotes);
        return false;
    }

    // Test full construction
    testSong = new Song('id', 'spotify', 'spotifyId', 1, 1);

    if (testSong.id != 'id') {
        console.log('testSong (full): invalid ID generated => ', testSong.id);
        return false;
    }
    if (testSong.platform != 'spotify') {
        console.log('testSong (full): incorrect platform => ', testSong.platform);
        return false;
    }
    if (testSong.platformSongId != 'spotifyId') {
        console.log('testSong (full): incorrect platformSongId => ', testSong.platformSongId);
        return false;
    }
    if (testSong.upvotes != 1) {
        console.log('testSong (full): incorrect upvotes => ', testSong.upvotes);
        return false;
    }
    if (testSong.downvotes != 1) {
        console.log('testSong (full): incorrect downvotes => ', testSong.downvotes);
        return false;
    }

    return true;
}

function testQueue() {
    // Create a bunch of dummy songs
    // let songs = [];
    // for (let i = 0; i < 3; i++) {
    //     songs.push(new Song(undefined, 'spotify', 'abcde0'+i, 0, 0));
    //     if (false) { // toggle print for test song info
    //         // songs[i].print();
    //     }
    // }

    // let qwayway = new Queue(undefined, songs);
    // qwayway.push(new Song('_AAAAAAAAA', 'NEW 1', 'FIRE MIX 1', 0, 0));
    // qwayway.push(new Song('_BBBBBBBBB', 'NEW 2', 'OKAY TRACK 2', 0, 0));
    // qwayway.printSongs();
    // console.log(" ---------- POP ----------");
    // qwayway.pop().print();
    // console.log(" ------- New Queue -------");
    // qwayway.printSongs();

    // console.log(' ------- Vote1 ------- ');
    // qwayway.vote('_AAAAAAAAA', 5);
    // qwayway.vote('_BBBBBBBBB', 3);
    // qwayway.printSongs();

    // console.log(' ------- Vote2 ------- ');
    // qwayway.vote('_BBBBBBBBB', 2);
    // qwayway.printSongs();

    // console.log(' ------- Vote3 ------- ');
    // qwayway.vote('_BBBBBBBBB', 2);
    // qwayway.printSongs();

    // console.log(" ---------- POP ----------");
    // qwayway.pop().print();
    // qwayway.printSongs();

    return true;
}

module.exports = test;
