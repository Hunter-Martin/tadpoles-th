# Design

## TODO:

1. Incorporate sockets into listening page workflow
2. Add get api endpoints

## Workflows

### Creating a Party

1. User clicks create party from homepage
2. User Authenticates with spotify
3. User is directed to Party Configuration page & they set settings
4. Frontend hits backend with CREATE PARTY request
5. Backend receives CREATE PARTY request
6. Backend creates the party model
7. Backend joins the DJ to the party
8. Backend sends a success message

### Join Listening Party
1. User clicks join party form homepage
2. User authenticates with spotify
3. User is directed to join page and enters party code
4. Frontend hits backend with JOIN PARTY request
5. Backend receives JOIN PARTY request
6. Backend updates party model & adds new member to members
7. Backend sends confimration with/as party object

### Add Song to Queue
1. User searches a song & select a song
2. Frontend sends required info to backend over socket
3. Backend creates and adds song to Queue of corresponding party
4. Backend sends updated queue to all members through sockets

### Vote on a Song in the Queue
1. User click upvote or downvote
2. Frontend sends required info to backend over socket
3. Backend adds the upvote or downvote value to the song on the queue
4. Backend sends updated queue to all members through sockets

## Models:

### Party List:
- Static Party List: Party[]

### Party:
- PK Party ID: ID
- Party Code: string
- DJ ID: string
- List of Users / members: string[]
- Queue: Queue Object
- Party Config: Party Config Object
- Currently Playing Song: Song Object
- Members: string[]

### Song:
- Song ID: ID
- Platform: string
- Platform ID: string
- upvotes: int
- downvotes: int

### Queue:
- PQ: Priority Queue \<Song>

### Party Config:
- Party name: string
- party size: int
- Queue size: int

## Networking

### API Endpoints
Necessary endpoints:
- Create a party (POST)
  - Party Creator ID
  - Party config details
- Join a party (PUT)
  - Party Joiner ID
  - Party Code

### Sockets
- Add to queue
  - Party ID
  - Song Platform
  - Song Platform ID
- Voting on queue
  - Party ID
  - Song ID
  - Vote value
- End of Song
  - Party ID

## Notes:

1. Sockets may need a secure SSH, possibly use SingalR or Socket.io
2. May have to make a cache

## Future Work:

1. Socket.io may not work over cellular 4g / 5g. A mobile version using API's may need to be created.

